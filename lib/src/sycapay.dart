import 'package:flutter/cupertino.dart';
import 'package:sycapay_sdk/src/api/api_client.dart';
import 'package:sycapay_sdk/src/constants/constants.dart';
import 'package:sycapay_sdk/src/models/syca_response.dart';
import 'package:sycapay_sdk/src/models/checkout.dart';

class Sycapay {
  static String _apiKey;
  static String _merchandId;
  static String _token;
  static String _currency;

  static void init({
    String apiKey,
    String merchandId,
    String currency = ApiConstants.XOF_CURRENCY,
  }) {
    _apiKey = apiKey;
    _merchandId = merchandId;
    _currency = currency;
  }

  static Future<int> login({@required double amount}) async {
    _checkCredentials();
    var sycaResponse = await ApiClient.login(
        amount: amount,
        currency: _currency,
        merchandId: _merchandId,
        apiKey: _apiKey);
    var responseCode = sycaResponse.code;
    if (responseCode == 0) {
      _token = sycaResponse.token;
    }
    return responseCode;
  }

  static Future<SycaResponse> checkout(
      {@required marchandid,
      @required telephone,
      @required name,
      @required pname,
      @required urlnotif,
      @required montant,
      @required currency,
      @required numcommande}) async {

    _checkCredentials();

    var checkout = Checkout(
        marchandid: marchandid,
        telephone: telephone,
        name: name,
        pname: pname,
        urlnotif: urlnotif,
        montant: montant,
        currency: currency,
        numcommande: numcommande,
        token: _token);

    return ApiClient.checkout(checkout: checkout);
  }

  static Future<SycaResponse> status({@required String reference}) async {
    _checkCredentials();
    return ApiClient.status(reference: reference);
  }

  static _checkCredentials() {
    if (_apiKey == null) {
      throw Exception(Strings.CREDENTIAL_MISSING
          .replaceAll(Strings.CREDENTIAL_VAR, "apiKey"));
    }

    if (_merchandId == null) {
      throw Exception(Strings.CREDENTIAL_MISSING
          .replaceAll(Strings.CREDENTIAL_VAR, "merchandId"));
    }

    if (_token == null) {
      throw Exception(Strings.TOKEN_MISSING);
    }
  }
}
