class ApiConstants {
  static const String JSON_DATA_FORMAT = "JSON";
  static const String XOF_CURRENCY = "XOF";
}

class Strings {
  static const String CREDENTIAL_VAR = "%credentials%";
  static const String CREDENTIAL_MISSING =
      "$CREDENTIAL_VAR non défini! Veuillez renseigner cette information en appelant la fonction Sycapay.init().";
  static const String TOKEN_MISSING =
      "aucun token trouvé! Veuillez en crée un en appelant la fonction Sycapay.login().";
}

class ApiUrls {
  static const String _BASE_URL = "https://dev.sycapay.com";
  static const String LOGIN_URL = "$_BASE_URL/login.php";
  static const String CHECKOUT_URL = "$_BASE_URL/checkoutpayapiJson.php";
  static const String STATUS_CHECKOUT_URL = "$_BASE_URL/GetStatus.php";
}
