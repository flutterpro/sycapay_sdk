import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:sycapay_sdk/src/models/syca_response.dart';
import 'package:sycapay_sdk/src/constants/constants.dart';
import 'package:sycapay_sdk/src/models/checkout.dart';

class ApiClient {
  static final http.Client _client = http.Client();

  static Future<SycaResponse> login(
      {@required double amount,
      String currency,
      String merchandId,
      String apiKey}) async {
    var response = await _client.post(ApiUrls.LOGIN_URL, body: {
      "amount": amount,
      "currency": currency
    }, headers: {
      "X-SYCA-MERCHANDID": merchandId,
      "X-SYCA-APIKEY": apiKey,
      "X-SYCA-REQUEST-DATA-FORMAT": ApiConstants.JSON_DATA_FORMAT,
      "X-SYCA-RESPONSE-DATA-FORMAT": ApiConstants.JSON_DATA_FORMAT
    });

    if (response.statusCode > 300) {
      throw Exception();
    }

    return SycaResponse.fromJson(jsonDecode(response.body));
  }

  static Future<SycaResponse> checkout({@required Checkout checkout}) async {
    var response = await _client.post(ApiUrls.CHECKOUT_URL,
        body: checkout.toJson(), headers: {"Content-Type": "application/json"});

    if (response.statusCode > 300) {
      throw Exception();
    }

    return SycaResponse.fromJson(jsonDecode(response.body));
  }

  static Future<SycaResponse> status({@required String reference}) async {
    var response = await _client.post(ApiUrls.STATUS_CHECKOUT_URL,
        body: {'ref': reference});

    if (response.statusCode > 300) {
      throw Exception();
    }

    return SycaResponse.fromJson(jsonDecode(response.body));
  }
}
