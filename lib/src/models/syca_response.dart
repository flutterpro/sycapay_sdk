class SycaResponse {
  int code;
  String token;
  String desc;
  String transactionId;
  String paimentId;
  String mobile;
  String orderId;
  String amount;

  SycaResponse(
      {this.code,
      this.token,
      this.desc,
      this.transactionId,
      this.paimentId,
      this.mobile,
      this.orderId,
      this.amount});

  SycaResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    token = json['token'];
    desc = json['desc'];
    transactionId = json['transactionId'];
    paimentId = json['paimentId'];
    mobile = json['mobile'];
    orderId = json['orderId'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['token'] = this.token;
    data['desc'] = this.desc;
    return data;
  }
}
