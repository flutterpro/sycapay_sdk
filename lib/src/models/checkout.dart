import 'package:flutter/foundation.dart';

class Checkout {
  String marchandid;
  String token;
  String telephone;
  String name;
  String pname;
  String urlnotif;
  String montant;
  String currency;
  String numcommande;

  Checkout({
    @required this.marchandid,
    @required this.telephone,
    @required this.name,
    @required this.pname,
    @required this.urlnotif,
    @required this.montant,
    @required this.currency,
    @required this.token,
    @required this.numcommande,
  });

  Checkout.fromJson(Map<String, dynamic> json) {
    marchandid = json['marchandid'];
    token = json['token'];
    telephone = json['telephone'];
    name = json['name'];
    pname = json['pname'];
    urlnotif = json['urlnotif'];
    montant = json['montant'];
    currency = json['currency'];
    numcommande = json['numcommande'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['marchandid'] = this.marchandid;
    data['token'] = this.token;
    data['telephone'] = this.telephone;
    data['name'] = this.name;
    data['pname'] = this.pname;
    data['urlnotif'] = this.urlnotif;
    data['montant'] = this.montant;
    data['currency'] = this.currency;
    data['numcommande'] = this.numcommande;
    return data;
  }
}
